#!/usr/bin/env bash
set -euxo pipefail

multi_property_file="multi-build.properties"

# path and projects as properties
. ${multi_property_file}

IFS=';' read -ra ADDR <<< "${projects}"
for current_project in "${ADDR[@]}"; do
  # create temp-folder and copy docker-files and start-script there 
  # why: to avoid wrong apps in wrong docker-files
  # we get multiple app.jar files
  tmp_folder=${PWD}/${current_project}
  mkdir ${tmp_folder}
  cp Dockerfile ${tmp_folder}/Dockerfile
  cp start-app.sh ${tmp_folder}/start-app.sh
  cp single-build-docker.sh ${tmp_folder}/single-build-docker.sh
  # expect java sources and gradle-script here
  current_project_path=${project_home}${current_project}
  (
    # go  into tmp-folder and run script
    cd ${tmp_folder}
    bash ./single-build-docker.sh ${current_project_path} ${1:-} ${2:-}
  )
  # cleanup temp folder
  rm -rf ${tmp_folder}
done
