#!/usr/bin/env sh
set -euxo pipefail

HOST="0.0.0.0"

# open java jmx port for tools like visual-vm
JMX="-Dcom.sun.management.jmxremote=true \
 -Dcom.sun.management.jmxremote.rmi.port=${jmx_port:-8082} \
 -Dcom.sun.management.jmxremote.port=${jmx_port:-8082} \
 -Dcom.sun.management.jmxremote.local.only=false \
 -Dcom.sun.management.jmxremote.authenticate=false \
 -Dcom.sun.management.jmxremote.ssl=false \
 -Dcom.sun.management.jmxremote.host=${HOST} \
 -Djava.rmi.server.hostname=${HOST} \
 -agentlib:jdwp=transport=dt_socket,address=${debug_port:-8081},server=y,suspend=n \
 -Dsun.management.jmxremote.level=FINEST \
 -Djava.util.logging.ConsoleHandler.level=FINEST \
 -Dsun.management.jmxremote.handlers=java.util.logging.ConsoleHandler"

# set memory limit for heap, seams to be good for GC to set max and min on same value
MEMORY="-Xmx${max_heap_space_mb}M \
 -Xms${max_heap_space_mb}M"

# set and show non-heap memory, especially in microservices with spring
# is non-heap size > heap size 
NATIVE_MEMORY="-XX:+UnlockExperimentalVMOptions \
 -XX:NativeMemoryTracking=summary \
 -XX:+HeapDumpOnOutOfMemoryError \
 -XX:NativeMemoryTracking=summary \
 -XX:HeapDumpPath=/data/heapdump-exec.hprof \
 -XX:ReservedCodeCacheSize=124m \
 -XX:CompressedClassSpaceSize=124m"

# configure spring
SPRING="-Dserver.port=${port} \
 -Dspring.cloud.config.profile=${spring_profile} \
 -Dspring.profiles.active=${spring_profile}"

if [ "${DEBUG:-}" == "true" ]
then
  # order matters!
  java ${MEMORY} ${NATIVE_MEMORY} -jar ${JMX} ${SPRING} /usr/local/app.jar
else
  # order matters!
  java ${MEMORY} -jar ${SPRING} /usr/local/app.jar
fi
