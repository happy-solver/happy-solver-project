#!/usr/bin/env bash
set -euxo pipefail

current_project_path=${1}
property_file=${current_project_path}/docker.properties
if [ ! -f $property_file ] 
then
  echo "File $property_file does not exist"
  echo "Example: $property_file"
  echo "repo=registry.gitlab.com/happy-solver"
  echo "name=happy-solver-server"
  echo "version=1.0.0"
  echo "jar=happy-solver-server.jar"
  exit 1
fi

# load properties
. $property_file

# create jar
is_compile=${2:-}
if [ "${is_compile}" = "compile" ]
then
  (
    cd ${current_project_path}
    ./gradlew clean bootJar
  )
fi

app_file=${current_project_path}/build/libs/$jar

if [ ! -f ${app_file}]
then
  echo "Need to compile java and create jar under ${app_file}"
  exit 1
fi

# copy jar to
cp ${app_file} app.jar
# build docker-image
docker build -t $repo/$name:$version .

is_push=${3:-}
if [ "${is_push}" == "push" ]
then
  docker push $repo/$name:$version
fi
# clean up (remove jar)
rm app.jar
