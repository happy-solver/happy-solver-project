# Happy-Solver-Project

This is overview and build repository

# Build images of all projects

Checkout all projects and set paths in `multi-build.properties`.

Compile all projects and create docker images:

```bash 
./multi-build-docker.sh compile
```
